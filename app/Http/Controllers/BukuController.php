<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Alert;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth']);
        $this->middleware(['checkRole:admin']);
    }
    public function index()
    {
        //Show Data Buku
        $data = DB::table('buku')->orderBy('judul_buku','asc')->get();
        return view('buku/master_buku',compact('data'));
    }

    public function create()
    {
        //Menuju halaman add buku
        return view('buku/add_buku');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate (
                [
                    'judul'=>'required',
                    'penulis'=>'required',
                    'penerbit'=>'required',
                    'tahun'=>'required',
                    'lokasi'=>'required',
                    'jumlah'=>'required',
                ]
            );
        //Insert data into database table buku
        $data = array(
            'judul_buku' => $request->judul,
            'penulis_buku' => $request->penulis,
            'penerbit_buku' => $request->penerbit,
            'tahun_terbitan' => $request->tahun,
            'lokasi' => $request->lokasi,
            'banyak_buku' => $request->jumlah,
            'buku_tersedia' => $request->jumlah,
            'cover' =>$request->file('img')->storePubliclyAs('',$request->judul.'_'.$request->tahun,'google'),
        );
        // $path = $request->file('img')->storeAs('',$request->judul,'google');
        
        // \Storage::disk('google')->put($request->judul.'.jpg',$request->file('cover'));

        DB::table('buku')->insert($data);
        \Alert::success('Buku Telah Ditambahkan');
        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file = \Storage::disk('google')->list();
        print_r($file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //Mengambil data buku dengan id_buku = $id
        $data = DB::table('buku')->where('id_buku',$id)->first();
        
        //Menuju halaman edit buku
        return view('buku/edit_buku',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = array(
            'judul_buku' => $request->judul,
            'penulis_buku' => $request->penulis,
            'penerbit_buku' => $request->penerbit,
            'tahun_terbitan' => $request->tahun,
            'lokasi' => $request->lokasi,
            'banyak_buku' => $request->jumlah,
            'buku_tersedia' => $request->tersedia
        );    

        DB::table('buku')->where('id_buku',$id)->update($data);

        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('buku')->where('id_buku',$id)->delete();
        Alert::success('Deleted');
        return \Redirect::back();
    }
}
