<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $files = \Storage::disk('google')->listContents();
        $img =[];
        $data = DB::table('buku')->orderBy('judul_buku','asc')->get();
        $name = array_column($files,'name');
        $base = array_column($files,'basename');
        foreach($data as $key => $value){
            if($value->cover){
                $id = array_search($value->cover,$name);
                $img[$key] = $base[$id];
                // $path[$key] = \Storage::disk('google')->files();
            }else{
                $img[$key] = Null;
            }
        }

        return view('index',compact('data','img'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $this->middleware(['auth']);
        //Get Specific User
        $user = DB::table('users')->where('id',\Auth::user()->id)->first();
        $data = DB::table('peminjaman_buku')->where('user_id',$user->id)->get();
        return view('/profile',compact('data','user'));
    
    }

    public function pinjam($id)
    {
        $this->middleware(['auth']);
        //Get Detail Peminjaman User    
        $data = DB::table('detail_peminjaman as pb')->where('id_peminjaman',$id)
                    ->join('buku','pb.id_buku','buku.id_buku')
                    ->get();
        return view('peminjaman',compact('data'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
