<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        //
        $this->middleware('auth');
    }
    public function index()
    {
        //Mengambil Data Cart User
        $user = \Auth::user()->id;
        $data = DB::table('cart')->where('id_user',$user)
                ->join('buku','buku.id_buku','cart.id_buku')
                ->get();
        foreach($data as $key=>$value){
            \Cart::session($user)->add(
                array(
                    'id'=>$value->id_buku,
                    'judul'=>$value->judul_buku,
                    'penulis'=>$value->penulis_buku,
                    'penerbit'=>$value->penerbit_buku,
                    'tahun'=>$value->tahun_terbitan,
                    'jumlah'=> 1,
                    )
                );

        }
        \Alert::success('Berhasil Login');
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //Memasukan Data Buku Kedalam Keranjan Peminjaman
        $data = DB::table('buku')->where('id_buku',$id)->first();
        $user = \Auth::user()->id;
        if(\Auth::user()){
            \Cart::session($user)->add(
                array(
                    'id'=>$data->id_buku,
                    'judul'=>$data->judul_buku,
                    'penulis'=>$data->penulis_buku,
                    'penerbit'=>$data->penerbit_buku,
                    'tahun'=>$data->tahun_terbitan,
                    'jumlah'=> 1,
                    )
                );
                DB::table('cart')->insert(['id_user'=>$user,'id_buku'=>$data->id_buku]);
                \Alert::success('Berhasil Dimasukan kedalam keranjang');
        } else {
            \Alert::error('Mohon Login');
        }
        $cart = \Cart::session($user)->getContent();

        // echo $cart;
        return \Redirect::back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Checkout Keranjang Peminjaman
        $user = \Auth::user()->id;
        if(\Auth::user()->status != 'Active'){
            \Alert::error('Akun Anda Belum Diaktivasi','Mohon Hubungi Admin Untuk Akativasi Akun');
            return \Redirect::back();
        }else{
            $data = \Cart::session($user)->getContent();
            $id = DB::table('peminjaman_buku')->insertGetId([
                'user_id'=>$user,
                'tgl_pinjam'=>Carbon::now(),
                'tgl_kembali'=>Carbon::now()->addDays(10),
                'jumlah_buku'=>\Cart::session($user)->getTotalQuantity()]);
            foreach($data as $key=>$value){
                DB::table('detail_peminjaman')->insert(['id_peminjaman'=>$id,'id_buku'=>$value->id,'tgl_pinjam'=>Carbon::now()]);
                DB::table('buku')->where('id_buku',$value->id)->decrement('buku_tersedia',1);
            }
            \Cart::session($user)->clear();
            DB::table('cart')->where('id_user',$user)->delete();
            \Alert::success('Berhasi Checkout','Mohon Hubungi Admin Untuk Mendapat Persetujuan');
            return redirect('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $data = \Cart::session(\Auth::user()->id)->getContent();
        return view('cart',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Hapus Buku Dari Cart
        $user = \Auth::user()->id;
        $buku = DB::table('buku')->where('id_buku',$id)->first();
        DB::table('cart')->where('id_user',$user)->where('id_buku',$id)->delete();
        \Cart::session($user)->remove($id);
        \Alert::success('Berhasil Dihapus Dari Keranjang',[$buku->judul_buku,$buku->penulis_buku,$buku->penerbit_buku]);

        return \Redirect::back();
    }
}
