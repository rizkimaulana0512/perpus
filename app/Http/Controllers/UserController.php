<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rules;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware(['auth']);
        $this->middleware(['checkRole:admin']);
    }
    public function index()
    {
        //get User Data
        $data = DB::table('users')->get();
        return view('user/master_user',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('user/add_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'nama' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);
        //Insert Data User
        $data = array(
            'name' => $request->nama,
            'email' => $request->email,
            'role' => $request->role,
            'status' => $request->status ?? 'Menunggu Persetujuan',
            'password' => \Hash::make($request->password)
        );

        DB::table('users')->insert($data);
        \Alert::success('User Berhasil Ditambahkan');

        return redirect('/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function aktivasi($id)
    {
        $data = DB::table('users')->where('id',$id)->update(['status'=>'Active']);

        \Alert::success('Akun Telah Diaktivasi');

        return \Redirect::back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edit User
        $data = DB::table('users')->where('id',$id)->first();

        return view('user/edit_user',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Update Data User
        $data = array(
            'name'=> $request->nama,
            'email' => $request->email,
            'role' => $request->role,
            'status' => $request->status ?? 'Menunggu Persetujuan',
        );

        if($request->password != ''){
            if($request->password == $request->confirm){
               $data = array_merge($data,[ 'password' => \Hash::make($request->password)]);
            }else
            {   
                \Alert::error('Password Tidak Cocok');
                return \Redirect::back();
            }
        };
        \Alert::success('Akun Berhasil Diedit');
        DB::table('users')->where('id',$id)->update($data);
        return redirect('/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Menghapus Data User
        DB::table('users')->where('id',$id)->delete();

        return redirect('/user');
    }
}
