<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware(['auth']);
        $this->middleware(['checkRole:admin']);
        
    }
    public function index()
    {
        //
        $data = DB::table('peminjaman_buku')
                    ->join('users','users.id','user_id')
                    ->select('*','peminjaman_buku.status as status_peminjaman')->get();

        return view('peminjaman/master_peminjaman',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = DB::table('buku')->get();

        return view('peminjaman/add_peminjaman',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Inset Data Peminjman
        $data = array(
            'user_id' => $request->nama,
            'tgl_pinjam' => $request->tgl,
            'tgl_kembali' => date_add(date_create($request->tgl),date_interval_create_from_date_string("10 days")),
            'jumlah_buku'=>0,
        );
        $id = DB::table('peminjaman_buku')->insertGetId($data);
        if($request->pinjam){
            foreach($request->pinjam as $key=>$value){
                DB::table('detail_peminjaman')->insert(['id_peminjaman'=>$id,'id_buku'=>$value,'tgl_pinjam'=>$request->tgl]);
                DB::table('peminjaman_buku')->where('id_peminjaman',$id)->increment('jumlah_buku',1);
                DB::table('buku')->where('id_buku',$value)->decrement('buku_tersedia',1);
            }
        }
        // echo count($request->pinjam);
        \Alert::success('Peminjaman Baru Berhasil Dibuat');
        return redirect('/peminjaman');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Melihat Detail Peminjaman
        $data = DB::table('detail_peminjaman as dp')->where('id_peminjaman',$id)
                    ->join('buku','buku.id_buku','dp.id_buku')
                    ->get();
        $status = DB::table('peminjaman_buku')->where('id_peminjaman',$id)->select('status','id_peminjaman')->first();

        return view('peminjaman/detail_peminjaman',compact('data','status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus($buku,$id){
        //Menolak dan Menghapus buku dari peminjaman
        DB::table('detail_peminjaman')
            ->where('id_peminjaman',$id)
            ->where('id_buku',$buku)
            ->delete();
        DB::table('peminjaman_buku')->where('id_peminjaman',$id)->decrement('jumlah_buku',1);

        //Mengembalikan Jumlah Buku
        DB::table('buku')->where('id_buku',$buku)->increment('buku_tersedia',1);
        \Alert::success('Buku Berhasil Dihapus Dari Peminjaman');
        return \Redirect::back();
    }

    public function selesai($id,$buku){
        //Buku Selesai Dipinjam
        DB::table('detail_peminjaman')
            ->where('id_peminjaman',$id)
            ->where('id_buku',$buku)
            ->update(['status'=>'Selesai','tgl_kembali'=>Carbon::now()]);

        //Mengembalikan Jumlah Buku
        DB::table('buku')->where('id_buku',$buku)->increment('buku_tersedia',1);
        \Alert::success('Buku Telah Diterima');
        return \Redirect::back();
    }

    public function tolak($id){
        DB::table('peminjaman_buku')->where('id_peminjaman',$id)->update(['status'=>'Ditolak']);
        DB::table('detail_peminjaman')->where('id_peminjaman',$id)->update(['status'=>'Ditolak']);
        $buku = DB::table('detail_peminjaman')->where('id_peminjaman',$id)->get();
        foreach($buku as $key=>$value){
            DB::table('buku')->where('id_buku',$value->id_buku)->increment('buku_tersedia',1);
        }
        \Alert::success('Peminjaman Ditolak');
        return \Redirect::back();
    }
    public function pinjam($jenis,$id)
    {
        //Meyetujui/Mengizinkan atau Menyelesaikan Peminjaman
        if($jenis == 'setuju'){
            
            //Mengubah Status Peminjaman Menjadi Disetujui
            DB::table('detail_peminjaman')->where('id_peminjaman',$id)->update(['status'=>'Disetujui']);
            DB::table('peminjaman_buku')->where('id_peminjaman',$id)->update(['status'=>'Disetujui']);
            \Alert::success('Peminjaman Disetujui');
            
        }else if($jenis =='selesai'){

            //Menambah Kembali Jumlah Buku Tersedia
            $data = DB::table('detail_peminjaman')->where('id_peminjaman',$id)->get();
            foreach($data as $key =>$value){
                if($value->status != 'Selesai'){
                    DB::table('buku')->where('id_buku',$value->id_buku)->increment('buku_tersedia',1);
                }
            }
            //Mengubah Status Peminjaman Menjadi Selesai
            DB::table('detail_peminjaman')->where('id_peminjaman',$id)->update(['status'=>'Selesai','tgl_kembali'=>Carbon::now()]);
            DB::table('peminjaman_buku')->where('id_peminjaman',$id)->update(['status'=>'Selesai']);

            \Alert::success('Peminjaman Selesai');
            
        }
        return \Redirect::back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //Mengambil Data User Dan Peminjaman Buku
        $data = DB::table('peminjaman_buku as pb')->where('id_peminjaman',$id)
                    ->join('users','users.id','pb.user_id')->select('*','pb.status as status_pinjam')->first();
        //Mengambil Data Buku
        $buku = DB::table('buku')->get();
        //Mengambil Detail Peminjaman
        $pinjam = DB::table('detail_peminjaman')->where('id_peminjaman',$id)->select('id_buku')->get()->all();
        return view('peminjaman/edit_peminjaman',compact('data','buku','pinjam'));
    

    }
    public function update(Request $request, $id)
    {
        //Update Data Peminjaman
        $data = array(
            'user_id' => $request->nama,
            'tgl_pinjam' => $request->tgl,
            'tgl_kembali' => date_add(date_create($request->tgl),date_interval_create_from_date_string("10 days")),
            'status'=>$request->status,
        );
        DB::table('peminjaman_buku')->where('id_peminjaman',$id)->update($data);

        //Update Data Detail Peminjaman
        $buku = DB::table('detail_peminjaman')->where('id_peminjaman',$id)->select('id_buku')->get();
        if($request->pinjam){
            $pinjam = array_diff(array_column(json_decode(json_encode($buku), true),'id_buku'),$request->pinjam);
            $aju = array_diff($request->pinjam,array_column(json_decode(json_encode($buku), true),'id_buku'));
          
            foreach($pinjam as $key=>$value){
                DB::table('buku')->where('id_buku',$value)->where('buku_tersedia','<','banyak_buku')->increment('buku_tersedia',1);
                DB::table('detail_peminjaman')->where('id_peminjaman',$id)
                    ->where('id_buku',$value)->delete();
                DB::table('peminjaman_buku')->where('id_peminjaman',$id)->decrement('jumlah_buku',1);

            }
            
            foreach($aju as $key=>$value){
                DB::table('detail_peminjaman')->where('id_peminjaman',$id)->insert(['tgl_pinjam'=>$request->tgl,'id_peminjaman'=>$id,'id_buku'=>$value,'status'=>$request->status]);
                DB::table('buku')->where('id_buku',$value)->decrement('buku_tersedia',1);
                DB::table('peminjaman_buku')->where('id_peminjaman',$id)->increment('jumlah_buku',1);
            }
            
        }else{
            DB::table('detail_peminjaman')->where('id_peminjaman',$id)->delete();
        }
     
        \Alert::success('Peminjaman Berhasil Diedit');
        return redirect('/peminjaman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Menghapus data Peminjaman
        $data = DB::table('peminjaman_buku')->where('id_peminjaman',$id)->first();
        if($data->status == 'Disetujui' || $data->status == 'Menunggu Persetujuan'){
            \Alert::error('Peminjaman Sedang Berjalan Atau Menunggu Persetujuan');
        }else{
            $buku = DB::table('detail_peminjaman')->where('id_peminjaman',$id)->get();
            foreach($buku as $key=>$value){
                if($value->status != 'Selesai'){
                    DB::table('buku')->where('id_buku',$value->id_buku)->increment('buku_tersedia',1);
                }
            }
            DB::table('peminjaman_buku')->where('id_peminjaman',$id)->delete();
            \Alert::success('Peminjaman Berhasil Dihapus');
        }
        return redirect('/peminjaman');
    }


    public function layout()
    {
    	return view('select2');
    }

    public function dataAjax(Request $request)
    {
    	$data = [];


        if($request->has('q')){
            $search = $request->q;
            $data = DB::table('users')->select("id","name")
            		->where('name','LIKE',"%$search%")
            		->get();
        }
        else{
            $data = DB::table('users')->select("id","name")
                    ->get();
        }

        return response()->json($data);
    }

}
