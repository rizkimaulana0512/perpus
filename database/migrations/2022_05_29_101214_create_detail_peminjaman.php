<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPeminjaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_peminjaman', function (Blueprint $table) {
            $table->foreignId('id_peminjaman')->references('id_peminjaman')->on('peminjaman_buku')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('id_buku')->references('id_buku')->on('buku')->cascadeOnDelete()->cascadeOnUpdate();
            $table->date('tgl_pinjam');
            $table->date('tgl_kembali')->nullable();
            $table->string('status')->default('Menunggu Disetujui');
            $table->timestamps();
            $table->primary(['id_peminjaman','id_buku']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_peminjaman');
    }
}
