<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
// use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Seeder to create admin account
        
        DB::table('users')->insert(['name'=>'admin','email'=>'admin@gmail.com','password'=>Hash::make('admin'),'role'=>'admin','status'=>'Active']);
        /** User::create(['name'=>'admin','email'=>'admin@gmail.com','password'=>Hash::make('admin'),'status'=>'admin']); */
        $buku = [
            ['One Piece','Eichiro Oda','Jepang',1990,'A30','10'],
            ['One Piece 2','Eichiro Oda','Jepang',1993,'A30','10'],
            ['One Piece 3','Eichiro Oda','Jepang',1995,'A30','10'],
            ['One Piece 4','Eichiro Oda','Jepang',1997,'A30','10'],
            ['Doraemon','Fujiko','Kyoto',1994,'A31','5'],
            ['Doraemon 2','Fujiko','Kyoto',1993,'A31','5'],
            ['Naruto ','Mashasi Kisamoto','Tokyo',1992,'A32','2'],
            ['Naruto 2','Mashasi Kisamoto','Tokyo',1996,'A32','2'],
        ];
        foreach($buku as $key=>$value){
            DB::table('buku')->insert(['judul_buku'=>$value[0],'penulis_buku'=>$value[1],'penerbit_buku'=>$value[2],'tahun_terbitan'=>$value[3],'lokasi'=>$value[4],'banyak_buku'=>$value[5],'buku_tersedia'=>$value[5]]);
        }
    }
}
