<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PeminjamanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Index/Home Route
Route::get('/',[HomeController::class,'index']);

//Route Buku
Route::get('/buku',[BukuController::class,'index']);
Route::get('/add_buku',[BukuController::class,'create']);
Route::post('/add_buku',[BukuController::class,'store']);
Route::get('/edit_buku/{id}',[BukuController::class,'edit']);
Route::post('/edit_buku/{id}',[BukuController::class,'update']);
Route::get('/delete_buku/{id}',[BukuController::class,'destroy']);
Route::get('/show/{id}',[BukuController::class,'show']);


//Route Cart
Route::get('/getcart',[CartController::class,'index']);
Route::get('/cart/{id}',[CartController::class,'create']);
Route::get('/cart',[CartController::class,'show']);
Route::get('/delete_cart/{id}',[CartController::class,'destroy']);
Route::post('/checkout',[CartController::class,'store']);

//Route User
Route::get('/user',[UserController::class,'index']);
Route::get('/add_user',[UserController::class,'create']);
Route::get('/edit_user/{id}',[UserController::class,'edit']);
Route::post('/add_user',[UserController::class,'store']);
Route::post('/edit_user/{id}',[UserController::class,'update']);
Route::get('/delete_user/{id}',[UserController::class,'destroy']);
Route::get('/aktivasi/{id}',[UserController::class,'aktivasi']);

//Route Peminjaman
Route::get('/peminjaman',[PeminjamanController::class,'index']);
Route::get('/add_peminjaman',[PeminjamanController::class,'create']);
Route::post('/add_peminjaman',[PeminjamanController::class,'store']);
Route::get('/edit_peminjaman/{id}',[PeminjamanController::class,'edit']);
Route::post('/edit_peminjaman/{id}',[PeminjamanController::class,'update']);
Route::get('/peminjaman/{id}',[PeminjamanController::class,'show']);
Route::get('/peminjaman/{jenis}/{id}',[PeminjamanController::class,'pinjam']);
Route::get('/selesai/{buku}/{id}',[PeminjamanController::class,'selesai']);
Route::get('/tolak/{buku}/{id}',[PeminjamanController::class,'hapus']);
Route::get('/tolak/{id}',[PeminjamanController::class,'tolak']);
Route::get('/delete_peminjaman/{id}',[PeminjamanController::class,'destroy']);

Route::get('/select2-autocomplete', [PeminjamanController::class,'layout']);
Route::get('/select2-autocomplete-ajax', [PeminjamanController::class,'dataAjax']);


//Route User Side
Route::get('/profile',[HomeController::class,'show']);
Route::get('/pinjam/{id}',[HomeController::class,'pinjam']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
