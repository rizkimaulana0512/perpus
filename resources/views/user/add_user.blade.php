@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded rounded">
                            <x-auth-validation-errors class="mb-4" :errors="$errors" />
                            <form action="/add_user" method="post">
                                @csrf
                                <div class="form-group">
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Nama</b>
                                            <input type="text" class="form-control" id="nama" name="nama" required
                                            placeholder="Nama">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Email</b>
                                            <input type="email" class="form-control" id="email" name="email" required
                                            placeholder="Email">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Role</b>
                                            <select class="form-control" name="role" id="role">
                                                <option value="admin">Admin</option>
                                                <option value="member">Member</option>
                                            </select>
                                        </p>
                                    </div>
                                    <div></div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Password</b>
                                            <input type="password" class="form-control" id="password" name="password" required
                                            placeholder="Password">
                                            <p class="tooltip">Password must at least 8 character</p>   
                                            <br>
                                            <a href="#" onclick="show()" class="text-dark" id="show">Show Password</a>
                                        </p>
                                        <div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Confirm Password</b>
                                            <x-input id="password_confirmation" class="form-control mt-1 w-full"
                                            type="password"
                                            name="password_confirmation" required />
                                        </p>
                                    </div>
                                    <p><b>Status</b>
                                            <p>
                                                <input class="form-check-input" type="checkbox" id="status" name="status" value="Active"> &nbspActive
                                            </p>
                                        </p>
                                </div>
                                <button class="ml-2" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
@push('script')
    <script>
        function show(){
            // document.getElementById('eye').className="fas fa-eye-slash";
            var show = document.getElementById('show');
            var pass = document.getElementById('password')
            var conf = document.getElementById('password_confirmation');
            if(show.innerHTML == "Show Password"){
                show.innerHTML="Hide Password"
                pass.type="text";
                conf.type="text";

            }else if(show.innerHTML == "Hide Password"){
                show.innerHTML="Show Password"
                pass.type="password"
                conf.type="password"
            }
        }
    </script>
@endpush