@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded text-center rounded">
                        <form action="/edit_user/{{$data->id}}" method="post">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Nama</b>
                                            <input type="text" class="form-control" id="nama" name="nama" value="{{$data->name}}"
                                            placeholder="Nama">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Email</b>
                                            <input type="email" class="form-control" id="email" name="email" value="{{$data->email}}"
                                            placeholder="Email">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Role</b>
                                            <select class="form-control" name="role" id="role">
                                                <option value="admin" {{$data->role == 'admin' ? 'selected' : ''}} >Admin</option>
                                                <option value="member" {{$data->role == 'member' ? 'selected' : ''}} >Member</option>
                                            </select>
                                        </p>
                                    </div>
                                    <div>

                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>New Pasword</b>
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Confirm Password</b>
                                        <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirm Password">
                                        </p>
                                    </div>
                                    <p><b>Status</b>
                                            <p>
                                                <input class="form-check-input" type="checkbox" id="status" name="status" value="Active" {{$data->status == 'Active' ? 'checked' : ''}}> &nbspActive
                                            </p>
                                        </p>
                                </div>
                                <button class="ml-2" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
