@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded text-center rounded">
                            <form action="/edit_buku/{{$data->id_buku}}" method="post">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Judul Buku</b>
                                            <input type="text" class="form-control" id="judul" name="judul" value="{{$data->judul_buku}}"
                                            placeholder="Judul Buku">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Penulis</b>
                                            <input type="text" class="form-control" id="penulis" name="penulis" value="{{$data->penulis_buku}}"
                                            placeholder="Penulis">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Penerbit</b>
                                            <input type="text" class="form-control" id="penerbit" name="penerbit" value="{{$data->penerbit_buku}}"
                                            placeholder="Penerbit">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Tahun Terbit</b>
                                            <input type="year" class="form-control" id="tahun" name="tahun" value="{{$data->tahun_terbitan}}"
                                            placeholder="Tahun Terbit">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Lokasi</b>
                                            <input type="text" class="form-control" id="lokasi" name="lokasi" value="{{$data->lokasi}}"
                                            placeholder="Lokasi">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Banyak Buku</b>
                                            <input type="number" class="form-control" id="jumlah" name="jumlah" value="{{$data->banyak_buku}}"
                                            placeholder="Banyak Buku">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Buku Tersedia</b>
                                            <input type="number" class="form-control" id="tersedia" name="tersedia" value="{{$data->buku_tersedia}}"
                                             placeholder="Buku Tersedia">
                                        </p>
                                    </div>
                                </div>
                                <button class="ml-2" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
