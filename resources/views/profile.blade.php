@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded text-center rounded">
                            <p>Nama : {{$user->name}} </p>
                            <p>Email :{{$user->email}}</p>
                            <table id="table1" class="table-bordered table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Id Peminjaman</th>
                                        <th>Tanggal Peminjaman</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Jumlah Buku</th>
                                        <th>Status</th>
                                        <th>Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key=>$value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$value->id_peminjaman}}</td>
                                        <td>{{$value->tgl_pinjam}}</td>
                                        <td>{{$value->tgl_kembali}}</td>
                                        <td>{{$value->jumlah_buku}}</td>
                                        <td>{{$value->status}}</td>
                                        <td> <a href="/pinjam/{{$value->id_peminjaman}}" class="btn btn-primary">Detail &nbsp<i class="fas fa-eye fa-sm text-white-10"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- <form action="/checkout" method="post">
                                @csrf
                                <button type="submit" class="btn btn-primary">Checkout</button>
                            </form> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
