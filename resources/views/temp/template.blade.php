<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Business Casual - Start Bootstrap Theme</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>
    <body>
    @include('sweetalert::alert')
        <header>
            <h1 class="site-heading text-center text-faded d-none d-lg-block">
                <span class="site-heading-upper text-primary mb-3">Perpustakaan</span>
                <span class="site-heading-lower">Universitas XYZ</span>
            </h1>
        </header>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <a  href="/" class=""><i class="fas fa-home-alt fa-sm text-white"></i></a>
                    @guest
                        <ul class="navbar-nav ms-auto">
                            <li class="nav-item px-lg-4">
                                <a href="{{ route('login') }}" class="nav-link text-uppercase" href="index.html">Log in</a>
                            </li>
                            <li class="nav-item px-lg-4">
                                <a href="{{ route('register') }}" class="nav-link text-uppercase" href="index.html">Register</a>
                            </li>
                        </ul>
                    @else
                        @if(Auth::user()->role=="admin")
                            <ul class="navbar-nav ms-auto ps-5">
                                <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="/buku">Buku</a></li>
                                <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="/peminjaman">Peminjaman</a></li>
                                <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="/user">User</a></li>
                            </ul>
                        @endif
                            <ul class="navbar-nav ms-auto">
                                <li class="nav-item px-lg-4">
                                    <a class="nav-link text-uppercase" href="/cart"><i class="fas fa-shopping-cart"></i>({{Cart::session(Auth::user()->id)->getTotalQuantity()}})</a>
                                </li>
                                <li  class="nav-item px-lg-4">
                                    <a id="navbarDropdown" class="nav-link text-uppercase navbar-toggle dropdown-toggle" href="#" role="button" data-bs-target="#dropdownMenu" data-bs-toggle="collapse" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }}
                                    </a>
                                    <ul id="dropdownMenu" class="dropdown-menu">
                                        <li>
                                            <a class="dropdown-item" href="/profile">Profile</a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
                                            
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                                                
                        </div>
                    @endguest
                </div>
            </div>
        </nav>
        @yield('content')
        <footer class="footer text-faded text-center py-5">
            <div class="container"><p class="m-0 small">Copyright &copy; Your Website 2022</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        
        <!-- Core theme JS-->
        <script src="{{asset('js/scripts.js')}}"></script>
        <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>

        @stack('script')
    </body>
</html>
