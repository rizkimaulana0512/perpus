@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <a href="/profile" class="btn btn-primary"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp Kembali</a>
                        <div class="cta-inner bg-faded text-center rounded">
                            <table id="table1" class="table-bordered table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Judul</th>
                                        <th>Penulis</th>
                                        <th>Penertbit</th>
                                        <th>Tahun Terbit</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Tanggal Dikembalikan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key=>$value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$value->judul_buku}}</td>
                                        <td>{{$value->penulis_buku}}</td>
                                        <td>{{$value->penerbit_buku}}</td>
                                        <td>{{$value->tahun_terbitan}}</td>
                                        <td>{{$value->tgl_pinjam}}</td>
                                        <td>{{$value->tgl_kembali}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
