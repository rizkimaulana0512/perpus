@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded rounded">
                             <!-- Session Status -->
                            <x-auth-session-status class="mb-4" :status="session('status')" />

                            <!-- Validation Errors -->
                            <x-auth-validation-errors class="mb-4" :errors="$errors" />
                            <form action="{{ route('login') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Email</b>
                                            <input type="email" class="form-control" id="email" name="email" required
                                            placeholder="Email">
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Password</b>
                                        <input type="password" class="form-control" id="password" name="password" required
                                        placeholder="Password">
                                        <a href="#" onclick="show()" class="text-dark" id="show">Show Password</a>
                                        </p>
                                    </div>
                                    <div>
                                    </div>
                                    <a class="btn btn-warning ml-2" type="button" href="/register">Register</a>
                                    <button class="btn btn-primary ml-2" type="submit">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
@push('script')
    <script>
        function show(){
            // document.getElementById('eye').className="fas fa-eye-slash";
            var show = document.getElementById('show');
            var pass = document.getElementById('password');
            if(show.innerHTML == "Show Password"){
                show.innerHTML="Hide Password"
                pass.type="text";

            }else if(show.innerHTML == "Hide Password"){
                show.innerHTML="Show Password"
                pass.type="password"
            }
        }
    </script>
@endpush
