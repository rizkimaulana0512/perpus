@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded rounded">
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <!-- Name -->
                            <div>
                                <x-label for="name" :value="__('Name')" />

                                <input id="name" class="form-control mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                            </div>

                            <!-- Email Address -->
                            <div class="mt-4">
                                <x-label for="email" :value="__('Email')" />

                                <x-input id="email" class="form-control mt-1 w-full" type="email" name="email" :value="old('email')" required />
                            </div>

                            <!-- Password -->
                            <div class="mt-4">
                                <x-label for="password" :value="__('Password')" />

                                <x-input id="password" class="form-control mt-1 w-full"
                                                type="password"
                                                name="password"
                                                required autocomplete="new-password" />
                                <p class="tooltip">Password must at least 8 character</p>
                            </div>
                            <div class="mt-4">
                                <a href="#" onclick="show()" class="text-dark" id="show">Show Password</a>
                            </div>
                            <!-- Confirm Password -->
                            <div class="mt-4">
                                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                                <x-input id="password_confirmation" class="form-control mt-1 w-full"
                                                type="password"
                                                name="password_confirmation" required />
                            </div>

                            <div class="flex items-center justify-end mt-4">
                                <a class="underline text-sm text-dark hover:text-gray-900" href="{{ route('login') }}">
                                    {{ __('Already registered?') }}
                                </a>
                            </div>
                                <x-button class="ml-4 btn btn-primary">
                                    {{ __('Register') }}
                                </x-button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
@push('script')
    <script>
        function show(){
            // document.getElementById('eye').className="fas fa-eye-slash";
            var show = document.getElementById('show');
            var pass = document.getElementById('password');
            if(show.innerHTML == "Show Password"){
                show.innerHTML="Hide Password"
                pass.type="text";

            }else if(show.innerHTML == "Hide Password"){
                show.innerHTML="Show Password"
                pass.type="password"
            }
        }
    </script>
@endpush