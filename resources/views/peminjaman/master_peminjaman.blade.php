@extends('temp/template')
@push('script')
<script src="{{asset('/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#table1").DataTable();
  });
</script>
@endpush
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded text-center rounded">
                            <a href="/add_peminjaman" class="btn btn-primary" style="text-align:left">
                                Tambah Data &nbsp
                                <i class="fas fa-plus"></i>
                            </a>
                            <table id="table1" class="table-bordered table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Jumlah Buku Dipinjam</th>
                                        <th>Tanggal Peminjaman</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key=>$value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->jumlah_buku}}</td>
                                        <td>{{$value->tgl_pinjam}}</td>
                                        <td>{{$value->tgl_kembali}}</td>
                                        <td>{{$value->status_peminjaman}}</td>
                                        <td>
                                                <a href="/peminjaman/{{$value->id_peminjaman}}" class="btn btn-primary">Detail &nbsp<i class="fas fa-eye fa-sm text-white-10"></i></a>
                                                <a href="/edit_peminjaman/{{$value->id_peminjaman}}" class="btn btn-primary">Edit &nbsp<i class="fas fa-pencil-alt fa-sm text-white-10"></i></a>
                                                <a href="/delete_peminjaman/{{$value->id_peminjaman}}" class="btn btn-danger">Delete &nbsp<i class="fas fa-pencil-alt fa-sm text-white-10"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
