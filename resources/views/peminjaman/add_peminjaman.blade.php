@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded text-center rounded">
                            <form action="/add_peminjaman" method="post">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Peminjam</b>
                                            <select class="nama form-control" name="nama" id="nama" required></select>
                                        </p>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0 ml-2">
                                        <p><b>Tanggal Peminjaman</b>
                                            <input type="date" class="form-control" id="tgl" name="tgl" value="<?php echo date('Y-m-d');?>" required
                                            placeholder="Tanggal Peminjaman">
                                        </p>
                                    </div>
                                </div>
                                <table id="table1" class="table-bordered table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Judul</th>
                                        <th>Penulis</th>
                                        <th>Penertbit</th>
                                        <th>Tahun Terbit</th>
                                        <th>Lokasi</th>
                                        <th>Buku Tersedia</th>
                                        <th>Peminjaman</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key=>$value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$value->judul_buku}}</td>
                                        <td>{{$value->penulis_buku}}</td>
                                        <td>{{$value->penerbit_buku}}</td>
                                        <td>{{$value->tahun_terbitan}}</td>
                                        <td>{{$value->lokasi}}</td>
                                        <td>{{$value->buku_tersedia}}/{{$value->banyak_buku}}</td>
                                        <td>@if($value->buku_tersedia == 0)
                                                Buku Tidak Tersedia
                                            @else
                                                <input type="checkbox" name="pinjam[{{$key}}]" value={{$value->id_buku}}>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                                <button class="ml-2" type="submit">Submit</button>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
@push('script')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
    var i = 0
    var jumlah=[]
    
    $('#nama').select2({
                    placeholder: 'Select an item',
                    ajax: {
                    url: '/select2-autocomplete-ajax',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                        results:  $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                    }
                });
    </script>
@endpush
