@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <a href="/peminjaman" class="btn btn-primary"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp Kembali</a>
                        <div class="cta-inner bg-faded text-center rounded">
                            <table id="table1" class="table-bordered table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Judul</th>
                                        <th>Penulis</th>
                                        <th>Penertbit</th>
                                        <th>Tahun Terbit</th>
                                        <th>Tanggal Pinjam</th>
                                        <th>Tanggal Dikembalikan</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key=>$value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$value->judul_buku}}</td>
                                        <td>{{$value->penulis_buku}}</td>
                                        <td>{{$value->penerbit_buku}}</td>
                                        <td>{{$value->tahun_terbitan}}</td>
                                        <td>{{$value->tgl_pinjam}}</td>
                                        <td>{{$value->tgl_kembali}}</td>
                                        <td>{{$value->status}}
                                            @if($value->status == 'Disetujui')
                                                <a href="/selesai/{{$status->id_peminjaman}}/{{$value->id_buku}}" class="btn btn-primary">Selesai &nbsp<i class="fas fa-pencil fa-sm text-white-10"></i></a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($value->status == 'Menunggu Disetujui')
                                                <a href="/tolak/{{$value->id_buku}}/{{$status->id_peminjaman}}" class="btn btn-danger">Hapus &nbsp<i class="fas fa-times fa-sm text-white-10"></i></a>
                                            @endif

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if($status->status == 'Menunggu Persetujuan')
                                <a href="/peminjaman/setuju/{{$status->id_peminjaman}}" class="btn btn-primary">Setujui &nbsp<i class="fas fa-pencil fa-sm text-white-10"></i></a>
                                <a href="/tolak/{{$status->id_peminjaman}}" class="btn btn-danger">Tolak &nbsp<i class="fas fa-times fa-sm text-white-10"></i></a>

                            @elseif($status->status == 'Disetujui')
                                <a href="/peminjaman/selesai/{{$status->id_peminjaman}}" class="btn btn-primary">Selesai &nbsp<i class="fas fa-pencil fa-sm text-white-10"></i></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
