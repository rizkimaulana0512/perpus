@extends('temp/template')
@section('content')
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded text-center rounded">
                            <table id="table1" class="table-bordered table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th></th>
                                        <th>Judul</th>
                                        <th>Penulis</th>
                                        <th>Penertbit</th>
                                        <th>Tahun Terbit</th>
                                        <th>Lokasi</th>
                                        <th>Buku Tersedia</th>
                                        <th></th>
                                        <th>Peminjaman</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key=>$value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>
                                            <img src="https://drive.google.com/uc?export=view&id={{$img[$key]}}" style="width:100px ; hegith:100px" alt="">
                                        </td>
                                        <td>{{$value->judul_buku}}</td>
                                        <td>{{$value->penulis_buku}}</td>
                                        <td>{{$value->penerbit_buku}}</td>
                                        <td>{{$value->tahun_terbitan}}</td>
                                        <td>{{$value->lokasi}}</td>
                                        <td>{{$value->buku_tersedia}}/{{$value->banyak_buku}}</td>
                                        <td>
                                            <a href="/show/{{$value->id_buku}}" class="btn btn-primary">PINJAM &nbsp<i class="fas fa-pencil-alt fa-sm text-white-10"></i></a>
                                        </td>
                                        <td>
                                            @if($value->buku_tersedia == 0)
                                                Buku Tidak Tersedia
                                            @else
                                                <a href="/cart/{{$value->id_buku}}" onclick="pinjam({{$value->id_buku}})" class="btn btn-primary">PINJAM &nbsp<i class="fas fa-pencil-alt fa-sm text-white-10"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop
@push('script')
<script src="{{asset('/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#table1").DataTable({
        "columnDefs": [{
             "searchable": false, "targets": [0,6] }
            ]
    });
  });
</script>
@endpush
